import React, {Component} from 'react'
import Header from '../../../components/Header/Header';
import Footer from '../../../components/Footer/Footer';

export class Dashboard extends Component {
    constructor(props) {
        super(props);

        let state = localStorage["appState"];
        let AppState = JSON.parse(state);

        this.state = {
            isLoggedIn: AppState ? AppState.isLoggedIn : false,
            user: AppState ? AppState.user : {}
        }
    }

    get user() {
        return this.state.user;
    }

    render() {
        return (
            <div>
                <Header userData={this.user} userIsLoggedIn={this.isLoggedIn}/>

                <span>Whatever normally goes into the user dasboard page; the table below for instance</span> <br/>

                <table className="table table-striped">
                    <tbody>
                    <tr>
                        <th scope="row ">User Id</th>
                        <td>{this.user.id}</td>
                    </tr>
                    <tr>
                        <th scope="row ">Full Name</th>
                        <td>{this.user.name}</td>
                    </tr>
                    <tr>
                        <th scope="row ">Email</th>
                        <td>{this.user.email}</td>
                    </tr>
                    </tbody>
                </table>

                <Footer/>
            </div>
        )
    }
}
